package com.example.simpsonsgrades

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun loginClick(view: View) {
        val name = name_text.text.toString()
        val password = password_text.text.toString()

        val fb = FirebaseDatabase.getInstance().reference
        val students = fb.child("students")
        val bart = students.orderByChild("name").equalTo(name)

        bart.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(data: DataSnapshot) {
                processData(name, password, data)
            }
        })
    }

    private fun processData(name: String, password: String, data: DataSnapshot) {

        if (!data.hasChildren()) {
            Toast.makeText(this, "No such user!", Toast.LENGTH_SHORT).show()
            return
        }

        val newData = data.children.iterator().next()
        val student = newData.getValue(Student::class.java)
        if (password == student!!.password && name == student!!.name) {
            val intent = Intent(this, GradesActivity::class.java)
            intent.putExtra("name", name)
            intent.putExtra("id", student.id)
            startActivity(intent)
        } else {
            Toast.makeText(this, "Wrong password!", Toast.LENGTH_SHORT).show()
        }
    }
}