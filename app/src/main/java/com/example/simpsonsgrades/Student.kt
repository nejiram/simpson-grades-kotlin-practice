package com.example.simpsonsgrades

data class Student (
    var email: String = "",
    var id: Int = 0,
    var name: String = "",
    var password: String = ""
)