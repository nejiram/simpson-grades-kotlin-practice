package com.example.simpsonsgrades

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_grades.*

class GradesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grades)

        val name = intent.getStringExtra("name")
        title_text.text = "$name's Grades"

        val id = intent.getIntExtra("id", 0)

        val fb = FirebaseDatabase.getInstance().reference
        val grades = fb.child("grades").orderByChild("student_id").equalTo(id.toDouble())

        grades.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(data: DataSnapshot) {
                processData(data)
            }
        })

    }

    private fun processData(data: DataSnapshot) {
        var list = mutableListOf<String>()
        for (child in data.children) {
            val grade = child.getValue(Grade::class.java)
            list.add(grade!!.grade + " in " + grade!!.course_name)
        }
        grades_list.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list)
    }
}